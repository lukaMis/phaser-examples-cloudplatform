
// IMPORT CUSTOM GAME OBJECTS
//@prepros-prepend customObjects/CloudPlatform.js



// IMPORT GAME STATES
//@prepros-prepend states/Boot.js
//@prepros-prepend states/Preloader.js
//@prepros-prepend states/MainMenu.js
//@prepros-prepend states/Game.js
//@prepros-prepend states/EndMenu.js



var game = new Phaser.Game(640, 480, Phaser.CANVAS, 'game');

game.state.add('Boot', cloudPlatform.Boot);
game.state.add('Preloader', cloudPlatform.Preloader);
game.state.add('MainMenu', cloudPlatform.MainMenu);
game.state.add('Game', cloudPlatform.Game);
game.state.add('EndMenu', cloudPlatform.EndMenu);
// game.state.add('EndMenu', cloudPlatform.EndMenu);


game.state.start("Boot");