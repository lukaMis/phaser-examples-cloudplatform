console.log("Boot log");

var cloudPlatform = cloudPlatform || {};


cloudPlatform.Boot = function (game) {};


cloudPlatform.Boot.prototype = {
  init: function () {
    this.game.renderer.renderSession.roundPixels = true; 
    this.world.resize(640*3, 480);
    this.physics.startSystem(Phaser.Physics.ARCADE);
    this.physics.arcade.gravity.y = 600;
    this.stage.backgroundColor = '#DDDDDD';
  },
  create: function () {
    game.state.start('Preloader');
  }
};