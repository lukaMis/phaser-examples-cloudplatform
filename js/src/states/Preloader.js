console.log("Preloader log");

var cloudPlatform = cloudPlatform || {};


cloudPlatform.Preloader = function (game) {};

cloudPlatform.Preloader.prototype.preload = function () {
  
  this.load.image('trees', 'assets/images/trees-h.png');
  this.load.image('background', 'assets/images/clouds-h.png');
  this.load.image('platform', 'assets/images/platform.png');
  this.load.image('cloud-platform', 'assets/images/cloud-platform.png');
  this.load.spritesheet('dude', 'assets/images/dude.png', 32, 48);
};


cloudPlatform.Preloader.prototype.create = function () {
  game.state.start('MainMenu');
};